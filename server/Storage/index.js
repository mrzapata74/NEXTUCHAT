//Importamos el módulo "File System (fs)" para interactuar con el sistema de archivos de nuestra PC, y el módulo "path" para las rutas. Con éste archivo (/Storage/index.js) se leerá y escribirá el archivo ".json" cada vez que sea necesario.
//Para leerlo y escribirlo se usarán 2 métodos "saveData" y "getData", los que recibirán ciertos parámetros que harán funcionar la intereacción.

var fs = require("fs")
    path= require('path')
    
//Creacion de 2 funciones saveData() y getData()
module.exports = {
    
    //datatype se refiere a que información se va a actualizar(mensajes o usuarios)
    //newData es el nuevo dato a ingresar
    //data, hace referencia a los datos viejos
    saveData: function(dataType, newData, data){
        //Evaluando el primer parámetro (datatype) puede ser user o messages, a partir de ésto se construye el path correspondiente
        var dataPath = dataType == 'users' ? __dirname + path.join('/data/users.json'):
                        __dirname + path.join('/data/messages.json')
        data.current.push(newData)
        return new promise(function(resolve, reject){
            fs.writeFile(dataPath, JSON.stringify(data),function(err){
                if(err) reject(err)
                resolve('OK')
            })
        })  
    },
    getData: function(dataType){    
        var dataPath = dataType =='users' ?
                        __dirname + path.join('/data/users.json'):
                        --dirnae + path.join('/data/messages.json')
        return new promise(function(resolve, reject){
            fs.readFile(dataPath, 'utf8', function(err, readData){
                if(err) reject(err)
                resolve(JSON.parse(readData))
            })
        })

    }
}

    
