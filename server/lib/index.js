var fs      = require('fs')
var path    = require('path')
var Storage = require('../Storage') //Se require accesar a esta ruta

//Función que captura todos los usuarios actualmente en línea

function deleteUsers(user, callback){
    Storage.getData('users') //Obtiene todos los usuarios del archivo users.json que están en línea
           .then(function(users){
            var resultUsers = users.current.filter(function (MapUser){
                return MapUser.nombre != user.nombre //Retorna los usuarios que sean distintos 
            })
            var userDataPath = path.join(__dirname, '../') + '/Storage/data/users.json' 
            fs.writeFile(userDataPath, JSON.stringify({current: resultUsers}), function(error){
                if(error) callback(error)
                callback(null, 'OK')
            })
    }).catch(function(err){
        callback(err)
        
    })
}

module.exports = deleteUsers