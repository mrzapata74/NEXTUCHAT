//Requerimos 'express' y asignamos a una variable "Router" la función Router() de 'express'

var express = require('express')
var Storage = require('../Storage')
var Router = express.Router()


//Creación de rutas que se encargarán del flujo de información entre el servidor y el cliente.

Router.get('/users', function(req, res){
    //Obteniendo Usuarios
    Storage.getData('users')
           .then(function(users){
            res.json(users)
    }).catch(function(error){
        res.sendStatus(500).json(error)
    })

})


Router.get('/messages', function(req, res){    
    //Obteniendo Mensajes
    Storage.getData('messages')
           .then(function(messages){
            res.json(messages)
    }).catch(function(error){
        res.sendStatus(500).json(error)
    })
    
})


Router.post('/users', function(req, res){
    var user = req.body.users
    Storage.getData('users')
           .then(function(users){
        return new Promise(function(resolve, reject){
            Storage.savedata('users', user, users)
            .then(function(messages){
                resolve(messages)
            }).catch(function(err){
                reject(err)
            })
        })
    }).then(function(message){
        res.json(message)
    }).catch(function(err){
        res.sendStatus(500).json(err)
    })
    
})


Router.post('/messages', function(req, res){
    var message = req.body.message
    Storage.getData('message')
           .then(function(message){
        return new Promise(function(resolve, reject){
            Storage.savedata('users', message, message)
            .then(function(messages){
                resolve(messages)
            }).catch(function(err){
                reject(err)
            })
        })
    }).then(function(message){
        res.json(message)
    }).catch(function(err){
        res.sendStatus(500).json(err)
    })
    
})


module.exports = Router



(function(document, window, undefine, $, io){
    (function(){
        return Chat = {
            apiUrl: '/chat',
            $userDataModal: $('#modalCaptura'),
            $btnMessage: $('#btnMessage'),
            $messageText: $('#messageText'),
            userName:'',
            socket: io(),

            //Función principal que ejecutará las otras funciones para el correcto funcionamiento.
            Init: function(){
            //Se crea la variable self y se le asigna "this" para mantener el contexto.
            //Esta función es el equivalente al método constructor de la programación orientada a objetos.
            var self = this 
            this.fetchUserInfo(function (user){ //Será la encargada de mostrar el modal y pedirle al usuario que ingrese un nombre.
            self.renderUser(user)//Agrega al usuario que acaba de unise a la conversación al documento HTM
            })
            this.watchMessages()
                
            self.socket.on('userJoin',function(user){
                self.renderUser(user)
            })
                
            self.socket.on('message', function(message){
                self.renderUser(message)
            })
    },
     
    fetchUserInfo: function(callback){
        var self = this
        this.$userDataModal.openModal()
        var $GuardaInfo = $('.guardarInfo')
            $GuardaInfo.on('click', function(){
                var nombre = $('.nombreUsuario').val()
                var user = [{nombre: nombre, img:'p2.png'}]
                self.socket.emit('userJoin', user[0])
                callback(user)

                self.joinUser(user[0])//Envia una petición al servidor de tipo POST, para notificarle que un nuevo usuario se unió a la conversación.

                self.userName = nombre
                self.$userDataModal.closeModal()
            })
        
        self.getInitialUsers()
    },
        
        
        getInitialUsers: function(){
            var self = this
            var endpoint = self.apiUrl + '/users'
            self.ajaxRequest(endpoint, 'GET', {})
                .done(function (data){
                    var users = data.current
                    self.renderUser(users)
                }).fail(function(err){
                    console.log(err)
                })
        },
        //ajaxRequest. se hace con el fin de que sea reutilizable
        ajaxRequest: function(ur, type, data){
            return $.ajax({
                url: url,
                type: type,
                data: data
            })
        },
        
            joinUsers: function (user){
                var self = this
                //endpoint almacena la variable que contiene la url base
                var endpoint = self.apiUrl + '/users'
                var userObj = {user: user}
                self.ajaRequest(endpoint, 'POST', userObj)
                  .done(function (confirm){
                    console.log(confirm)
                }).fail(function(error){
                    alert(error)
                })
            },
                
            //Función renderUsers(), la cual recibe un arreglo como parámetros
                renderUsers: function(users){
                    var self = this
                    var userList = $('.users-list')
                    var userTemplate = '<li class=""collection-item avatar>' + 
                                        '<img src="image/:image:" class="circle">' + 
                                        '<span class="title">:nombre:</span>' + 
                                        '<p><img src="image/online.png"/>En Línea</p>' + 
                                        '</li>'
                    user.map(function (user){
                        var newUser = userTemplate.replace(':image:', 'p2.jpg')
                                                    .replace(':nombre:', user.nombre)
                    })
                },
                watchMessage: function(){
                    var self = this
                    self.$messageText.on('keypress', function(e){
                        if(e.which == 13){
                            if($(this).val().trim()!=''){
                                var message = {
                                    sender: self.userName,
                                    text:$(this).val()
                                }
                                self.rendemMessage(message)
                                self.soket.emit('message', message)
                                $(this).val('')
                            }else{
                                e.preventDefault()
                            }
                        }
                    })
                    self.$btnMessages.on('click', function(){
                        if(self.$messageText.val()!=''){
                            var message = {
                                sender: self.userName,
                                text: $(this).val()
                            }
                            self.rendemMessage(message)
                            self.socket.emit('message', message)
                            self.$messageText.val('')
                        }
                    })
                },
                renderMessage: function(message){
                    var self = this
                    var tipoMensaje = message.sender == self.userName ? 'recibidos':'enviados'
                    var messageList = $('.historial-chat')
                    var messageTemplate = '<div class=":tipoMensaje:">' + 
                                                '<div class="mensaje">'+
                                                    '<div class="imagen">'+
                                                      '<img src="image/p2.jpg" alt="Contacto"/>'+
                                                    '</div>' +
                                                    '<div class="texto">'+
                                                          '<span class="nombre">:nombre:</span></br>'+
                                                          '<span>:mensaje:</span>'+
                                                    '</div>' + 
                                                    '<div class="hora">'+
                                                       '<span class="numHora">:hora:</span>'+
                                                    '</div>'+
                                                '</div>'+
                                           '</div>';    
                    
                    var currentDate = new Date()
                    var newMessage = messageTemplate.replace(':tipoMensaje:', tipoMensaje)
                                                    .replace(':nombre:', message.sender)
                                                    .replace(':mensaje:', message.text) 
                                                    .replace(':hora:', currentDate.getHours()+ :, currentDate.getMinutes())
                    
                    messageList.append(newMessage)
                    $('.scroller-chat').animate({scrollTop: $('.scroller-chat').get(0).scrollHeight}, 500)
                       
                }
    
      }  
    })()
        Chat.Init()
})(document, window, undefined, jQuery, io)


