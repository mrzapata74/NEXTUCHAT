//Construyendo e iniciando el servidor usando 'express' y el módulo 'http'

var bodyParser  = require('body-parser'),
    http        = require('http'),
    express     = require('express'),
    chat        = require('/Chat'),
    socketio    = require('socket.io')
     
var port        = port = process.env.PORT || 3000,
    app         = express(),
    Server      = http.createServer(app),
    io          = socketio(Server)

//Ahora hay que instalar las dependencias del proyecto
//en el archivo package.json, usando:
//npm install o en su forma abareviada npm -i (Esto en la Consola de Node.js)

//Agregar dependencias en el archivo package.json

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use('./chat', chat)
app.use(express.static('public'))

Server.listen(port, function(){
    console.log("El servidor está corriendo en el puerto "+port)
})

io.on('Connection', function(socket){
    console.log('Nuevo Usuario Conectado: '+ socket.id)
    
    socket.on('userJoin', function(user){
        //Escuchar el evento user join para agregar un usuario y emitirlo a los otros sockets
        socket.user
        socket.broadcast.emit('userJoin', user)
        
    })
    
    socket.on('message', function(message){
        //Escuchar el evento message para emitirlo a los otros sockets
        socket.broadcast.emit('message', message)
        
    })
    
    socket.on('disconnect', function(){
        if(socket.hasOwnProperty('user')){
            deleteUsers(socket.user, function(err, confirm){
                if(err) throw err
            })
        }
        
    })
    
    
    
    
    
})



